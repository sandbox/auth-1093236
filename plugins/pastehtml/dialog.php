<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>{#pastehtml.title}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="text/javascript" src="<?php print substr_replace(strrev(strstr(strrev($_SERVER['SCRIPT_FILENAME']), strrev('/sites/'))), '', 0, strlen($_SERVER['DOCUMENT_ROOT'])) . 'all/libraries/tinymce/jscripts/tiny_mce/tiny_mce_popup.js';?>"></script>
	<script type="text/javascript" src="js/dialog.js"></script>
</head>
<body onresize="PasteTextDialog.resize();" style="display:none; overflow:hidden;">
	<form name="source" onsubmit="return PasteHtmlDialog.insert();" action="#">
		<div class="title">{#pastehtml.title}</div>

		<div>{#pastehtml.help}</div>

		<textarea id="content" name="content" rows="15" cols="100" style="width: 100%; height: 100%; font-family: 'Courier New',Courier,mono; font-size: 12px;" dir="ltr" wrap="soft" class="mceFocus"></textarea>
		<div class="mceActionPanel">
			<div style="float: left">
				<input type="submit" name="insert" value="{#insert}" id="insert" />
			</div>

			<div style="float: right">
				<input type="button" name="cancel" value="{#cancel}" onclick="tinyMCEPopup.close();" id="cancel" />
			</div>
		</div>
	</form>
</body>
</html>
