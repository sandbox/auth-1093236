
var PasteHtmlDialog = {
	init : function() {
		this.resize();
tinyMCEPopup.requireLangPack();
	},

	insert : function() {
		tinyMCEPopup.editor.execCommand('mceInsertPasteHtmlContent', false, {node: document.getElementById('content'), content : document.getElementById('content').value});
		tinyMCEPopup.close();
	},

	resize : function() {
		var vp = tinyMCEPopup.dom.getViewPort(window), el;

		el = document.getElementById('content');

		el.style.width  = (vp.w - 20) + 'px';
		el.style.height = (vp.h - 90) + 'px';
	}
};

tinyMCEPopup.onInit.add(PasteHtmlDialog.init, PasteHtmlDialog);
