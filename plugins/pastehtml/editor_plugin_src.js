/**
 *
 * @author Thomas Augustsson, a lot copied from paste from Moxiecode
 * @copyright Copyright � 2004-2008, Moxiecode Systems AB, All rights reserved.
 */

(function() {
	// Load plugin specific language pack
	tinymce.PluginManager.requireLangPack('pastehtml');

	tinymce.create('tinymce.plugins.PastehtmlPlugin', {
		init : function(ed, url) {
			var _this = this;
			_this.editor = ed;
			_this.url = url;

			// Add command for external usage
			ed.addCommand('mceInsertPasteHtmlContent', function(u, o) {
				_this.editor.execCommand('mceInsertContent', false, o.content);
				_this.editor.execCommand('mceCleanup');
			});

			ed.addCommand('mcePasteHtml', function() {
				_this.editor.windowManager.open({
					file : _this.url + '/dialog.php',
					width : parseInt(ed.getParam("paste_dialog_width", "450")),
					height : parseInt(ed.getParam("paste_dialog_height", "400")),
					inline : 1
				});
			});

			// Register buttons for backwards compatibility
			ed.addButton('pastehtml', {title : 'pastehtml.desc', cmd : 'mcePasteHtml', image : _this.url + '/img/pastehtml.gif'});
		},

		getInfo : function() {
			return {
				longname : 'Paste html',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('pastehtml', tinymce.plugins.PastehtmlPlugin);
})();
